# CA3 - Virtual Machines - part 2

![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange) ![alt badgedevops](https://img.shields.io/badge/devops--blue)

- [Part 1 - Create a Virtual machine with Ubuntu -- Virtualbox](../part1/readme.md#part1)
- [Part 2 - Create a Vagrantfile to work with Virtualbox provider](#part21)
- [Part 3 - Alternative Solution](#part31)
    - [Part 3.1 - Create a Vagrantfile to work with Hyper-V provider](#part31)
    - [Part 3.2 - Create a Vagrantfile to work with VMware provider](#part32)
- [Extra - Tomcat - Create users and permissions](#part4)
---
<a id="part21"></a> 

## Part 2 

# Create a Vagrantfile to work with Virtualbox provider

<br>
<sub><sup><em>PROVIDER - virtualbox</em></sub></sup><br>

![logo virtualbox](logo_virtualbox.png)



<sub><sup><em>OPERATING SYSTEM - ubuntu</em></sub></sup><br>

![logo ubuntu](logo_ubuntu.png)

<sub><sup><em>VAGRANT BOX - [app.vagrantup.com/boxes](https://app.vagrantup.com/boxes/search)</em></sub></sup><br>
#### hashicorp/bionic64 
<sub><sup><kbd>hyperv</kbd> <kbd>virtualbox</kbd> <kbd>vmware_desktop</kbd></sub></sup>

---
To configure the project via Vagrantfile, i wanted to work with:

- Ubuntu 18.04 - hashicorp/bionic64
- jdk 11 - openjdk-11-jdk-headless

<sub><sup><em>click at the link to to find a box</em></sub></sup>
[https://app.vagrantup.com/boxes/search](https://app.vagrantup.com/boxes/search)





Find a box from hashicorp and compatible with ubuntu 18.04
```bash
hashicorp/bionic64
```
![ca2 part2 vagrant](ca3_part2.1-vagrant.png)
 
### Create Vagrantfile

Provide some needed information: 
config.vm.box = "hashicorp/bionic64"
config.vm.provider "virtualbox"
config.vm.network
config.vm.provision

<sub><sup><em>click at the link to open the file</em></sub></sup>
[CA3 part2 Vagrantfile](./vagrant-v1-demo/Vagrantfile)

### Start installation of the virtual machine
Virtualbox is the default provider no need to specify
```bash
vagrant up --provision
```
<br>

![ca2 part2 running](ca3_part2.1-virtualbox_running_v2.png)

### Open virtual machine in ssh terminal
No password needed at this level
<sub><sup><em>Default username and password : vagrant</em></sub></sup>
```bash
vagrant ssh web
```

### Open web machine
hostnames : localhost | 192.168.56.10 | web | web.local<br>
port : 8080

[http://localhost:8080](http://localhost:8080)

[http://192.168.56.10:8080](http://192.168.56.10:8080)

### Open db machine
hostnames : localhost | 192.168.56.11 | db | db.local<br>
port : 8082

[http://localhost:8082](http://localhost:8082)

[http://192.168.56.11:8080](http://192.168.56.11:8080)

[http://192.168.56.10:8080/h2-console](http://192.168.56.10:8080/h2-console)

<br>

---

<a id="part31"></a> 

## Part 3.1 - alternative
# Create a Vagrantfile to work with hyper-v provider

<br>
<sub><sup><em>PROVIDER - hyperv</em></sub></sup><br>

![logo hyperv](logo_hyperv.png)

<br>
<sub><sup><em>OPERATING SYSTEM - ubuntu</em></sub></sup><br>

![logo ubuntu](logo_ubuntu.png)

<sub><sup><em>VAGRANT BOX - [app.vagrantup.com/boxes](https://app.vagrantup.com/boxes/search)</em></sub></sup><br>
#### hashicorp/bionic64
<sub><sup><kbd>hyperv</kbd> <kbd>virtualbox</kbd> <kbd>vmware_desktop</kbd></sub></sup>

---
> Because we choose a compatible box with virtualbox and hyper-v now is more easy to change for a alternative provider


### Enable Hyper-V on Windows 8.1 Pro or later
Windows Enterprise, Professional, or Education 8.1
```application.properties
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

<br>
Hyper-V to work as expected we need to change the spring.datasource.url from ip to the machine hostname
<br>
<sub><sup>file: <em>src > main > resources > application.properties</em></sub></sup>
```application.properties
spring.datasource.url=jdbc:h2:tcp://db:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
```


### Update Vagrantfile

<sub><sup><em>click at the link to open the file</em></sub></sup>
[CA3 part3.2 Vagrantfile](./vagrant-v2.2-hyperv/Vagrantfile)

```Vagrantfile
config.vm.provider "hyperv"
```

<br>

<sub><sup>(If not started yet)</sub></sup>
Start Windows Hyper-V server 


```bash
set-service  vmms -startuptype automatic
start-service -name vmms
```

### Create virtual machine
```bash
vagrant up --provision --provider=hyperv
```
<br>

![ca2 part2.2 running](ca3_part2.2-hyperv_manager.png)

In this image we can see the generated ip address for the web machine - 172.20.194.169

### Open virtual machine in ssh terminal
No password needed at this level
<sub><sup><em>Default username and password : vagrant</em></sub></sup>
```bash
vagrant ssh web
```

### Open web machine
hostnames : localhost | web | web.local<br>
port : 8080

[http://localhost:8080](http://localhost:8080)

[http://web:8080](http://web:8080)

[http://localhost:8080/basic-0.0.1-SNAPSHOT](http://localhost:8080/basic-0.0.1-SNAPSHOT)

[http://web:8080/basic-0.0.1-SNAPSHOT](http://web:8080/basic-0.0.1-SNAPSHOT)


### Open db machine
hostnames : localhost | 192.168.56.11 | db | db.local<br>
port : 8082

[http://localhost:8082](http://localhost:8082)

[http://db:8082](http://db:8082)


---

<a id="part32"></a> 

## Part 3.2 - alternative
# Create a Vagrantfile to work with VMware provider

<br>
<sub><sup><em>PROVIDER - vmware</em></sub></sup><br>

![logo vmware](logo_vmware.png)

<sub><sup><em>OPERATING SYSTEM - ubuntu</em></sub></sup><br>

![logo ubuntu](logo_ubuntu.png)

<sub><sup><em>VAGRANT BOX - [app.vagrantup.com/boxes](https://app.vagrantup.com/boxes/search)</em></sub></sup><br>
#### hashicorp/bionic64
<sub><sup><kbd>hyperv</kbd> <kbd>virtualbox</kbd> <kbd>vmware_desktop</kbd></sub></sup>

---

> Because we choose a compatible box with virtualbox and hyper-v now is more easy to change for a alternative provider


### Install Vagrant VMware desktop plugin
```application.properties
vagrant plugin install vagrant-vmware-desktop
```

<br>
VMware to work as expected we need to change the spring.datasource.url from ip to the machine hostname<br>
VMware does not recognized custom ip addresses.
<br>
<sub><sup>file: <em>src > main > resources > application.properties</em></sub></sup>
```application.properties
spring.datasource.url=jdbc:h2:tcp://db.local:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
```


### Update Vagrantfile

<sub><sup><em>click at the link to open the file</em></sub></sup>
[CA3 part3.2 Vagrantfile](./vagrant-v2.3-vmware/Vagrantfile)

for some reason it´s necessary to configure ethernet slot in each machine, otherwise is very instable

![logo ubuntu](ca3_part2.3-vmware_warning.png)

```Vagrantfile
...
config.vm.provider "vmware_desktop"

config.vm.provider "vmware_desktop" do |vmware|
    vmware.vmx["ethernet0.pcislotnumber"] = "32"
end
...
```
or
```Vagrantfile
...
config.vm.define "db" do |db|
    # Set more ram memmory for this VM
    db.vm.provider "vmware_desktop" do |v|
      v.vmx["ethernet0.pcislotnumber"] = "32"
    end
...
config.vm.define "web" do |web|
    # Set more ram memmory for this VM
    web.vm.provider "vmware_desktop" do |v|
      v.vmx["memsize"] = 1024
      #v.vmx["numvcpus"] = "2"
      v.vmx["ethernet0.pcislotnumber"] = "32"
    end
...
```


### Create virtual machine
```bash
vagrant up --provision --provider=vmware_desktop
```
<br>

![ca2 part2.2 running](ca3_part2.3-vmware_running.png)

### Open virtual machine in ssh terminal
No password needed at this level
<sub><sup><em>Default username and password : vagrant</em></sub></sup>
```bash
vagrant ssh web
```

### Open web machine
hostnames : localhost | web | web.local<br>
port : 8080

[http://localhost:8080](http://localhost:8080)

[http://web:8080/basic-0.0.1-SNAPSHOT](http://web:8080)

[http://web.local:8080/basic-0.0.1-SNAPSHOT](http://web.local:8080)


### Open db machine
hostnames : localhost | db | db.local<br>
port : 8082

[http://db:8082](http://localhost:8082)

[http://db.local:8082](http://db.local:8082)

[http://localhost:8082](http://localhost:8082)

[http://web:8080/basic-0.0.1-SNAPSHOT/h2-console](http://web:8080/basic-0.0.1-SNAPSHOT/h2-console)

[http://web.local:8080/basic-0.0.1-SNAPSHOT/h2-console](http://web.local:8080/basic-0.0.1-SNAPSHOT/h2-console)

---

<br>
<sub><sup><em>APACHE - TOMCAT 9</em></sub></sup><br>

<a id="part4"></a> 

![logo tomcat](logo_tomcat.png)

# TOMCAT - Create User permissions

### Open web server via ssh
```bash
vagrant ssh web
```
### Change tomcat users file
```bash
sudo vi /var/lib/tomcat9/conf/tomcat-users.xml
```

<br>
<sub><sup>file: <em>var > lib > tomcat9 > conf > tomcat-users.xml</em></sub></sup>
```xml
  ...
  <role rolename="tomcat"/>
  <role rolename="admin-gui"/>
  <role rolename="manager-gui"/>
  <user username="admin" password="admin" roles="tomcat,manager-gui,admin-gui"/>
</tomcat-users>
```