# CA3 - Virtual Machines - part 1

![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange) ![alt badgedevops](https://img.shields.io/badge/devops--blue)

- [Part 1 - Create a Virtual machine with Ubuntu -- Virtualbox](#part1)
- [Part 2 - Create a Vagrantfile to work with Virtualbox provider](../part2/readme.md#part21)
- [Part 3 - Alternative Solution](../part2/readme.md#part3)
    - [Part 3.1 - Create a Vagrantfile to work with Hyper-V provider](../part2/readme.md#part31)
    - [Part 3.2 - Create a Vagrantfile to work with VMware provider](../part2/readme.md#part32)

---
<a id="part1"></a> 

## Part 1

Create a Virtual machine with Ubuntu
===================
#### PROVIDER :: Virtualbox

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Virtualbox_logo.png/120px-Virtualbox_logo.png?20150209215936" alt="logo virtualbox" style="height: 80px;"/>


---
## Host Machine specifications
- Processor - Intel(R) Core(TM) i7-6700HQ CPU @ 2.60GHz   2.59 GHz
- Install RAM - 16GB

Windows specifications
- Edition - Windows 10
- Version - 21H2

---

## Virtual Machine specifications
- Install RAM - 2GB
- Network Adapter 1 - Nat
- Network Adapter 1 - Host-only Adapter (vboxnet0)

Linux specifications
- Edition - Ubuntu 
- Version - 18.04


Connect image (ISO) with the Ubuntu 18.04 minimal installation
[link](https://help.ubuntu.com/community/Installation/MinimalCD)

---

## Network Adapter Manager
- IPv4 Address - 192.168.56.1/24
- IPv4 Network Mask - 255.255.255.0

---

## Start Virtual Machine
### Update the packages repositories
```bash
sudo apt update
```
<br>

### Install the network tools
Edit the network configuration file to setup the IP
```bash
sudo apt install net-tools
sudo nano /etc/netplan/01-netcfg.yaml
```

```bash
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: yes
    enp0s8:
      addresses:
        - 192.168.56.5/24
```
Apply the new changes
```bash
sudo netplan apply
```
<br>

### Install openssh-server
```bash
sudo apt install openssh-server
```

Enable password authentication for ssh
```bash
sudo nano /etc/ssh/sshd_config
```
- uncomment the line PasswordAuthentication yes

Restart ssh service
```bash
sudo service ssh restart
```
<br>

### Install ftp server
```bash
sudo apt install vsftpd
```

Enable write access for vsftpd
```bash
sudo nano /etc/vsftpd.conf
```
- uncomment the line write_enable=YES

Restart service
```bash
sudo service vsftpd restart
```
<br>

---
## Connect to the virtual machine in the windows terminal
```bash
ssh joaoreis@192.168.56.5
```

### Install some applications

Git - Version Control System
```bash
sudo apt install git
```

Java Platform, Standard Edition 11 Development Kit (JDK 11)
```bash
sudo apt install openjdk-11-jdk-headless
```

<br>

---
## Clone private repository
Clone bitbucket private repository devops-21-22-atb-1211775
```bash
git clone https://joaoreisweb@bitbucket.org/joaoreisweb/devops-21-22-atb-1211775.git
```
---

<br><br><br><br>

---
## CA1
go to folder
```bash
cd ~/devops-21-22-atb-1211775/CA1/tut-react-and-spring-data-rest
```

add execution (775) permission to file
```bash
chmod +x mvnw
```

Start the server on project folder (<kbd>Ctrl</kbd> + <kbd>C</kbd> to Stop the app)
```bash
./mvnw spring-boot:run
```
- Ctrl + C to Stop the server

![ca1](ca1_running.png)

---

<br><br><br><br>

---
## CA2 - part1
go to folder
```bash
cd ~/devops-21-22-atb-1211775/CA2/part1/master
```
add execution (775) permission to file
```bash
chmod +x gradlew
```

Build project
```bash
./gradlew clean build
```
![ca2 part1](ca2_part1.1.png)

Start server (<kbd>Ctrl</kbd> + <kbd>C</kbd> to Stop the server)
```bash
./gradlew runServer
```
![ca2 part1](ca2_part1.2.png)

Try to run the client on virtual machine, turn into error
<br>
- Need to run client on a terminal with user interface changing 'localhost' to '192.168.56.5'
```bash
./gradlew runClient
```
![ca2 part1](ca2_part1.3.png)

---

## Run a client
Open another terminal on another computer with UI (User Interface) and execute the following gradle task from the project's root directory:
build.grade
```gradle
task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on 192.168.56.5:59001 "
    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}
```

<br><br><br><br>

---
## CA2 - part2 - gradle
go to folder
```bash
cd ~/devops-21-22-atb-1211775/CA2/part2-gradle/react-and-spring-data-rest-basic
```

add execution (775) permission to file
```bash
chmod +x gradlew
```

Start the server on project folder (<kbd>Ctrl</kbd> + <kbd>C</kbd> to Stop the app)
```bash
./gradlew build
```
![ca2 part1](ca2_part1.1.png)

---

<br><br><br><br>

---
## CA2 - part2 - maven
go to folder
```bash
cd ~/devops-21-22-atb-1211775/CA2/part2-maven/basic
```

add execution (775) permission to file
```bash
chmod +x mvnw
```

Start the server on project folder (<kbd>Ctrl</kbd> + <kbd>C</kbd> to Stop the app)
```bash
./mvnw spring-boot:run
```
![ca2 part2 maven](ca2_part2.1-maven.png)
![ca2 part2 maven](ca2_part2.2-maven.png)
