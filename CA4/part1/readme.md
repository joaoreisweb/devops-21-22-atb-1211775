# CA4 - Container with Docker - part 1

![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange) ![alt badgedevops](https://img.shields.io/badge/devops--blue)

- [Part 1.1 - Create chat application - cloning jar file](#part11)
- [Part 1.2 - Create chat application - copying jar file](#part12)
- [Part 1.3 - App running on server and client side](#part13)
- [Part 1.4 - Push to the hub](#part14)



![logo virtualbox](logo_docker.png)

---

<br>

<a id="part11"></a> 

## Part 1.1

# Create chat server in a container - cloning jar file


###Create a Dockerfile

<br>
<em>Dockerfile - header option 1 </em><br>

- Install from ubuntu image and add all java jdk dependencies separately

```Dockerfile

FROM ubuntu:18.04
RUN apt-get update -y
RUN apt-get install -y openjdk-8-jdk-headless
RUN apt-get install git -y
```
<br>
<em>Dockerfile - header option 2</em><br>

- Install from a image with embedded java jdk

```Dockerfile

FROM openjdk:11
```

<br>
<em>Dockerfile - option 2</em><br>

- Next, set the work directory, clone the repository, build java app and run ChatServerApp on port 59001

```Dockerfile

ENV HOME /root

WORKDIR $HOME

RUN git clone https://bitbucket.org/luisnogueira/gradle_basic_demo.git

WORKDIR  $HOME/gradle_basic_demo

RUN chmod u+x gradlew
RUN ./gradlew clean build

EXPOSE 59001

CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

```
<br>
<em>Meaning</em><br>

- ><b>FROM</b> :: Instruction initializes a new build stage and sets the Base Image

- ><b>RUN <command></b> :: Shell form, the command is run in a shell, which by default is :: <em>/bin/sh -c</em>

- ><b>ENV</b> :: Set a new global variable

- ><b>WORKDIR</b> :: Set the new work directory (change directory)

- ><b>EXPOSE</b> :: Container’s service can be connected to via port 59001 (TCP protocol default)

- ><b>CMD</b> :: Command that the container executes by default when you launch the built image

<br>

---

###Build image from docker file
<br>
<em>Command line</em><br>

```bash
docker build --no-cache -t greatest-job:20220512 .
```
<br>
<em>Meaning</em><br>

- ><b>build</b> :: Process of building Docker images using a Dockerfile

- ><b>-t</b> :: Flag tag (image) [name:tag] [greatest-job:20220512] format

- ><b>--no-cache</b> :: Force to update dependencies

- ><b>.</b> :: Path to the Dockerfile

<br>
<em>Result</em>

![logo docker](docker_images_v2_.png)

---

###Create and run a container

We can have multiple containers of same image, but only one can run at same time because they have the same port 59001

<em>Command line</em><br>

```bash
#1 option - with name
docker run -d -p 59001:59001  --name greatest-job-name greatest-job:20220512

#2 option - with random name
docker run -d -p 59001:59001  greatest-job:20220512
```
<br>
<em>Meaning</em><br>

- ><b>-d, --detach</b> :: detach (working in the background)

- ><b>-p, --publish</b> :: Map port 59001 of the host to port 59001 in the container

- ><b>--name</b> :: #1 Assign a name to the container "chat-container" (#2 or Docker automatically assigns the container name "pedantic_khayyam")

- ><b>greatest-job</b> :: The image to use (last argument)

<br>
<em>Result</em>

![logo docker](docker_containers_v2.png)

---

<br>

<a id="part12"></a> 
## Part 1.2

##Create chat application - copying jar file

###Create a Dockerfile

<br>
<em>Folder Structure</em><br>

```Folder
+-- part12
|   +-- build
|       +-- basic_demo-0.1.0.jar
|   +-- Vagrantfile
```

<br>
<em>Dockerfile - Install from a image with</em><br>

```Dockerfile

FROM openjdk:11

ENV HOME /root

WORKDIR $HOME

COPY build/basic_demo-0.1.0.jar .

EXPOSE 59001

CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

```

<br><br>

---

<a id="part13"></a> 
## Part 1.3

##App running on server side and client side

Using a local built, we can start multiple windows on client side
<br>
<em>Command line</em><br>

```bash
java -cp build/basic_demo-0.1.0.jar basic_demo.ChatClientApp localhost 59001
```

![logo docker](docker_chat_app.png)


<br><br>

---

<a id="part14"></a> 
## Part 1.4

##Push to the hub

```bash
docker login
```
Authenticating with existing credentials...
Login Succeeded

```bash
docker tag greatest-job2:20220512 joaoreisweb/devops:chatapp
```

```bash
docker push joaoreisweb/devops:chatapp
```

The push refers to repository [docker.io/joaoreisweb/devops]
b38fcfc1ce3d: Pushed
f62fbea9da0b: Pushed
5f70bf18a086: Pushed
3403b466bec9: Pushed
cbd16b72e80e: Pushed
07df3576ab77: Pushed
3b2356b88239: Pushed
a7934564e6b9: Pushed
1b7cceb6a07c: Pushed
b274e8788e0c: Pushed
78658088978a: Pushed
chatapp: digest: sha256:a17bf98fb9c9836e1b245dfa50c055f1f65ab6d929fbe55ce7cee0847829282e size: 2839

![logo hub](docker_hub.png)