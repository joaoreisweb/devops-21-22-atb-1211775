# CA4 - Container with Docker - part 2

![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange) ![alt badgedevops](https://img.shields.io/badge/devops--blue)

- [Part 2.1 - Create files](#part21)
  - [Part 2.1.1 - Docker Compose](#part211)
  - [Part 2.1.2 - Dockerfile to create the db docker image](#part212)
  - [Part 2.1.3 - Dockerfile to create the web docker image](#part213)
- [Part 2.2 - Push to the hub](#part22)
- [Part 2.3 - Copy db file to volume sync folder](#part23)



![logo virtualbox](logo_docker.png)

---

<br>

<a id="part21"></a> 

## Part 2.1

## Create files

<br>
<em>Folder Structure</em><br>

```Folder
+-- part2
|   +-- build (optional)
|       +-- basic_demo-0.1.0.jar
|   +-- db
|       +-- Dockerfile
|   +-- web
|       +-- Dockerfile
|   +-- docker-compose.yml
```
<a id="part211"></a> 

###Docker Compose

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services.

- **Services** - Each container is defined as a service inside the dockerfile-compose.

- **Containers** - The resective container for this service will be built using the configuration (a Dockerfile) inside a folder with the of the service

- **Volumes** - Are one or multiple share folders between the host and the container

- **Networks** - Specify custom networks


<em><sub>docker-compose.yml</sub></em>
```yaml
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24
```

---

<a id="part212"></a> 

## DB docker image

###Dockerfile to create the db docker image

<em><sub>db / Dockerfile</sub></em>

```Dockerfile
# install image from hub-docker
FROM ubuntu:18.04

# install all dependencies to run the application
RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

# create app folder
RUN mkdir -p /usr/src/app

# go to app folder
WORKDIR /usr/src/app/

# download h2 repository
RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

# open ports for public access
EXPOSE 8082
EXPOSE 9092

#
CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

```

---

<a id="part213"></a> 

## WEB docker image

###Dockerfile to create the web docker image

<em><sub>web / Dockerfile</sub></em>

```Dockerfile
# install image from hub-docker
FROM tomcat:9.0.48-jdk11-openjdk-slim

# install all dependencies to run the application
RUN apt-get update -y
RUN apt-get install -f
RUN apt-get install git -y
RUN apt-get install nodejs -y
RUN apt-get install npm -y

# create temp folder to build 
RUN mkdir -p /tmp/build

# go to temp folder
WORKDIR /tmp/build/

# Clone repository 
RUN git clone https://joaoreisweb@bitbucket.org/joaoreisweb/devops-21-22-atb-1211775.git

# go to basic folder
WORKDIR /tmp/build/devops-21-22-atb-1211775/CA4/part2/basic

# add write permissions to the gradlew
RUN chmod u+x gradlew

# wrap 
RUN ./gradlew clean build

# 
RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

#  
RUN rm -Rf /tmp/build/

# 
EXPOSE 8080

```

---

<br>

##Build and run the application

<em><sub>Command Line</sub></em>

```bash
# Option 1 - Create container but build if not exists
docker-compose up
# Option 2 - Create container but force build if not exists
docker-compose up --build
# Option 3 - Build image and then create the container and run it
docker-compose build
docker-compose up
```

![docker images](docker_images.jpg)

<br>

##Create and run the application

<em><sub>Command Line</sub></em>

```bash
docker-compose up
```
![docker containers](docker_containers.jpg)


<br>

---

<a id="part22"></a> 

## Part 2.2

## Push to the hub

Push the images to the hub.docker.com

[hub.docker.com](https://hub.docker.com/)


---

<br>

####Login to the Docker desktop

First you must login into Docker Desktop

<em><sub>Command Line</sub></em>

```bash
docker login
```
<em><sub>Docker desktop login page</sub></em>
![docker images](docker_login.jpg)

---

<br>

####Create tag to push the images to hub.docker.com

<em><sub>Command Line</sub></em>

```bash
## create new tag from an existing image
docker tag part2_db:latest joaoreisweb/devops:part2_db
docker tag part2_web:latest joaoreisweb/devops:part2_web
```

####Send all tag images to the hub

<em><sub>Command Line</sub></em>

```bash
## push all tags
docker push joaoreisweb/devops --all-tags
```

![docker images](docker_hub_push.jpg)


<br>

---

<a id="part23"></a> 

## Part 2.3

## Copy db file to volume sync folder

<em><sub>Command Line</sub></em>

```bash
docker ps
```
<em><sub>Command Line - Response</sub></em>

```bash
CONTAINER ID   IMAGE       COMMAND                  CREATED       STATUS         PORTS                                            NAMES
8fe37bf6056a   part2_web   "catalina.sh run"        2 hours ago   Up 3 minutes   0.0.0.0:8080->8080/tcp                           part2-web-1
6700150cc71e   part2_db    "/bin/sh -c 'java -c…"   2 hours ago   Up 3 minutes   0.0.0.0:8082->8082/tcp, 0.0.0.0:9092->9092/tcp   part2-db-1
```
<em><sub>Command Line</sub></em>

```bash
docker exec -it 6700150cc71e bash
```

<em><sub>SSH Container root - Command Line</sub></em>

```bash
root@6700150cc71e:/usr/src/app# ls -ls
```

<em><sub>SSH Container root - Command Line - Response</sub></em>

```bash
total 2280
2252 -rw-r--r-- 1 root root 2303679 Oct 14  2019 h2-1.4.200.jar
  28 -rw-r--r-- 1 root root   28672 May 19 17:43 jpadb.mv.db
```

<em><sub>SSH Container root - Command Line - copy the app **db** file to the volume sync folder between the host and the container</sub></em>

```bash
root@6700150cc71e:/usr/src/app# cp /usr/src/app/jpadb.mv.db /usr/src/data-backup
```

![docker copy](docker_copy.jpg)