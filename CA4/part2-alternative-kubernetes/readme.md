# CA4 - Kubernetes (K8S) - part 2 - alternative

![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange) ![alt badgedevops](https://img.shields.io/badge/devops--blue)

- [Part 2 - Kubernetes](#part30)
    - [Part 2.1 - What is kubernetes ?](#part21)
    - [Part 2.2 - What is Container Runtime Interface?](#part22)
    - [Part 2.3 - What is POD ?](#part23)
    - [Part 2.4 - What is Cluster ?](#part23)
    - [Part 2.5 - What is Service ?](#part23)
    - [Part 2.6 - Conclusion](#part23)
- [Part 3 - Implementation](#part30)
    - [Part 3.1 - Install tools](#part31)
    - [Part 3.2 - Update docker-compose.yml](#part32)
    - [Part 3.3 - Convert files with kompose](#part33)
    - [Part 3.4 - Create or update resources](#part34)
    - [Part 3.5 - Fetching the minikube IP](#part35)
    - [Part 3.6 - Several other commands](#part36)
    - [Part 3.7 - Errors](#part37)
    - [Part 3.8 - Running kubernetes - web and db pod](#part38)
    - [Part 3.9 - Conclusion](#part39)




<br>

![logo virtualbox](logo_kubernetes.jpg)

---


<a id="part21"></a> 

## Part 2.1

## What is kubernetes ?

Kubernetes, also known as K8s, is an open-source system for automating deployment, scaling, and management of containerized applications.

> **Kubernetes is container orchestration system**

It can orchestrate different containers on different physical or virtual servers all automatically, based on small configurations.

- **Automatic deployment** of the containerized applications across different servers (virtual servers more common)

- **Distribution of the load** across multiple servers (use resources more efficiently) avoid over or under allocation of any resources

- **Auto-scaling** of the deployed applications (quantity of the containers for each server)

- **Monitoring** and **health check** of the containers

- **Replacement** of the failed continers

---

<a id="part22"></a> 

## Part 2.2

## What is Container Runtime Interface?

The container runtime is the low-level component that creates and runs containers.
Kubernetes support several container runtime, such as:

- Docker - [docker.com](https://www.docker.com/)
- CRI-O - [cri-o.io](https://cri-o.io/)
- Containerd - [containerd.io](https://containerd.io/)

![kubernetes containers runtime](kubernetes.jpg)

---


<a id="part23"></a> 

## Part 2.3

## What is POD ?

> **Is the smallest unit in the kubernetes world**

Containers are created inside of the POD.

POD is like an aggregate that can have one or multiple containers, shared volumes and shared ip´s addresses.

![kubernetes containers runtime](kubernetes_anatomy_pod.jpg)

> **1 POT 1 SERVER**


---


<a id="part24"></a> 

## Part 2.4

## What is Cluster ?

Kubernetes **cluster** contains multiple **Nodes** (Nodes are servers)
Nodes of the same cluster are near each other because performance and effective performance.

> Inside of the **NODE** we have **POD**

Comunications are made from **Master Node** managed and distirbute job for all **Worker nodes**.

> All **POD** are deploy in worker nodes.

> Master Node only add **SYSTEM POD** cluster configurations.


---


<a id="part25"></a> 

## Part 2.5

## What is Service ?

- kubectl (command line tool - manage remotely kubernete cluster by rest api over protocol https)

Master Node

- **Api Server** (Main Server managing entire cluster)

- **Scheduler** (playing distributions nodes in the cluster)

- **Kube Controller Manager** (Controls everything that happens in the cluster)

- **Cloud Controller Manager** (Managing cloud service provider were cluster run from cloud provider - load balances)

- **etcd** Stores all logs operations cluster

- **dns** Name resolution for the cluster - comunication by domain name

- All Worker Node Services

Worker Node

- **kubelet** (Responsible inside comunication and between nodes)

- **kube-proxy**

- **Container Runtime** (run containers inside each node)

--- 

<a id="part26"></a> 

## Part 2.6

## Conclusion

The Kubernetes architecture consists of clusters and the cluster consists of nodes. 

One of the nodes is the master node that manages the other nodes called worker nodes. 

On each node there are POD´s, and the pod´s are created automatically by kubernetes. 

Inside the POD there are containers, usually only one container per POD, but you can have multiple ones, also the containers share volumes and network ip´s. 

The POD's are the smallest unit of the Kubernetes architecture.

Communication is done via a rest api within the cluster (as inter-cluster communication) via the kubectl (kube control) command line tool.

<br>

---

<a id="part30"></a> 

## Part 3.0

## Implementation

---

<a id="part31"></a> 

## Part 3.1

## Install tools

kubectl - The Kubernetes command-line tool
minikube - Runs a single-node Kubernetes cluster locally
kompose - Conversion tool for all things compose (namely Docker Compose) to container orchestrators (Kubernetes)

- Tools download - windows
![minikube](kubernetes_dependencies.jpg)

### Install kubectl

[kubectl install page](https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/)
kubectl binary with curl on Windows
```bash
curl -LO "https://dl.k8s.io/release/v1.24.0/bin/windows/amd64/kubectl.exe"
```

Verify your instalation
```bash
kubectl version --client --output=yaml
```

On windows download and install 

### Install Minikube

[minikube install page](https://minikube.sigs.k8s.io/docs/start/)
Create a cluster
```bash
minikube start

# minikube dashboard
minikube dashboard
```

![minikube](kubernetes_minikube_start.jpg)

### Install kompose

[kompose install page](https://kompose.io/installation/#windows)

<em>Kompose can be installed via [Chocolatey](https://community.chocolatey.org/packages/kubernetes-kompose)</em>

windows command line
```bash
choco install kubernetes-kompose
```

### Check tools instalation validating versions

![kubernetes tools versios](kubernetes_tools_versions.jpg)


---

<a id="part32"></a> 

## Part 3.2

## Udpate docker-compose.yml

```bash
services:
    web:
        labels:
            kompose.service.type: NodePort
        ...
    db:
        labels:
            kompose.service.type: NodePort
        ...
```

---

<a id="part33"></a> 

## Part 3.3

## Convert files with kompose

Convert docker-compose file  to container orchestrators such as Kubernetes

```bash
kompose --file docker-compose.yml convert
```
![kubernetes convert](kubernetes_kompose_convert.jpg)


---

<a id="part34"></a> 

## Part 3.4

## Create or update resources

```bash
## kompose.service.type: nodeport 
kubectl apply -f db-service.yaml,web-service.yaml,db-deployment.yaml,db-claim0-persistentvolumeclaim.yaml,default-networkpolicy.yaml,web-deployment.yaml

## kompose.service.type: loadbalancer 
kubectl apply -f db-tcp-service.yaml,web-tcp-service.yaml,db-deployment.yaml,db-claim0-persistentvolumeclaim.yaml,default-networkpolicy.yaml,web-deployment.yaml
```

![kubernetes convert](kubernetes_create_resources.jpg)


---

<a id="part35"></a> 

## Part 3.5

## Fetching the minikube IP

```bash
minikube service web --url
minikube service db --url
```

---

<a id="part36"></a> 

## Part 3.6

## Several other commands

```bash

## List - nodes - pods - services - deployment
kubectl get nodes

# Show only new pods
kubectl get pods
kubectl get services
kubectl get deployment

# View replicas
kubectl get rs
kubectl get replicasets
kubectl describe replicasets

## Delete
kubectl delete deployments web
kubectl delete deployments db
kubectl delete services web
kubectl delete services db

# Describe commands with verbose output
kubectl describe svc web
kubectl describe svc db

# Check the location and credentials that kubectl knows about
kubectl config view
# Using kubectl proxy 
kubectl proxy --port=8080
# explore the API 
http://localhost:8080

```

list logs
![kubernetes logs](kubernetes_logs.jpg)


---

<a id="part37"></a> 

## Part 3.7

## Errors - ImagePullBackOff - ErrImagePull

The ImagePullBackOff error occurs when the image path is incorrect, the network fails, or the kubelet does not succeed in authenticating with the container registry. Kubernetes initially throws the ErrImagePull error, and then after retrying a few times, “pulls back” and schedules another download attempt. For each unsuccessful attempt, the delay increases exponentially, up to a maximum of 5 minutes.

To identify the ImagePullBackOff error: run the kubectl get pods command. The pod status will show the error like so:

![kubernetes errors](kubernetes_error.jpg)


---

<a id="part38"></a> 

## Part 3.8

## Running kubernetes

Start minikube dashboard
```bash
minikube start
minikube dashboard
```
Create resources from hub-docker upload images in previous assignments

![kubernetes set db machines](kubernetes_minikube_set_db_machine.jpg)

![kubernetes set web machines](kubernetes_minikube_set_web_machine.jpg)


## minikube dashboard running resources

![kubernetes minikubes running](kubernetes_minikube_dashboard.png)

## easy to scale 

![kubernetes minikubes running](kubernetes_minikube_scale.jpg)

## minikube get services with external ip

```bash
minikube tunnel
```

![kubernetes minikubes running](kubernetes_logs_running.jpg)

## minikube dashboard running web machine - tomcat

![minikubes running web](kubernetes_minikube_running_web.jpg)

## minikube dashboard running web machine - h2 database

The web machine are missing url ajustment to work
Is trying to connect to ip **192.168.33.11:9092** and not to **localhost:9092**

![minikubes running web](kubernetes_minikube_running_db.jpg)

---


<a id="part39"></a> 

## Part 3.9

## Conclusions


In this implementation at the beginning it was very difficult to understand what few tools were needed. In the first approach, I wrongly try to convert docker-compose, it was incompatible ([kompose conversion](https://github.com/kubernetes/kompose/blob/master/docs/conversion.md)) with some features, and the result was two errors (ImagePullBackOff - ErrImagePull) without image pull.
After exploring the minikube dashboard, it was easy to deploy the hub image from the UI, a way to achieve the goal of getting the docker image running.