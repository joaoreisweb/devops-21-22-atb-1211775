# CA1 - Class Assignment 1 #



![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange)
![alt badgedevops](https://img.shields.io/badge/devops--blue)

- [Part 1 - Add new feature to the application with no branches](#part1)
- [Part 2 - Add new feature to the application with branches](#part2)
- [Part 3 - Alternative Solution](#part3)
    - [Part 3.1 - Add new feature to the application with no branches](#part31)
    - [Part 3.2 - Add new feature to the application with branches](#part32)



[//]: # (João Reis.)
<br>

# <a id="part1"></a> 

# Part 1 - Add new feature to the application with no branches
## Create employee new field to record years of the employee in the company
<br>



### _Go to folder_ 

_CA1 / tut-react-and-spring-data-rest / basic_

<br>

### _Mark application version v1.1.0_ (major.minor.revision)

```git
git tag -a v1.1.0 -m "initial version 1.1.0"
git push origin v1.1.0
```


### _Verify remote tags_ 
```git
git ls-remote --tags origin
```

### - Change the files to add the new field
- main/java/com.greglturnquist.payroll/
    - DatabaseLoader
    - Employee 
- main/js/api/
    - app.js

### _Commit changes of the files_ 
```git
git status
git add .
git commit -a -m "add jobYears field (see #1)"
git push origin master
```

## Acceptance criteria 
- The field jobYears must only accept integer values
### - Generate all needed tests and verify that all tests passed
- Validate all attributes to pass null/empty values

### _Commit the tests of all employee attributes_ 
```git
git status
git add .
git commit -a -m "Tests created for the employee class (see #1)"
git push origin master
```

<br>

## Build and RUN server on project folder
windows
```bash
mvnw spring-boot:run
```
linux
```bash
./mvnw spring-boot:run
```

<br>
Go to url

```url
https://localhost:8080
```

## Debug
- Verify if the application run without errors in your favourite browser
  - Inspect on browser console for errors

### _Commit changes and tag the new version_ 
```git
git status
git add .
git commit -a -m "jobYears feature completed - tested and debugged (see #1)"
git tag -a v1.2.0 -m "jobYears feature added 1.2.0"
git push origin v1.2.0
git tag -a ca1-part1 -m "End of the assignement (close #1)"
git push origin ca1-part1
git ls-remote --tags origin
```


<br><br>

# <a id="part2"></a> 

# Part 2 - Add new feature to the application with branches
## Create employee new field email for the employee in the company

<br>

### _Go to folder_ 
_CA1 / tut-react-and-spring-data-rest / basic_

<br>

### _Create and change to the new branch email-field_
```git
git checkout -b email-field
```

### _Verify actual working branch_
```git
git branch
```

### - Change the files to add the new field
- main/java/com.greglturnquist.payroll/
    - DatabaseLoader
    - Employee 
- main/js/api/
    - app.js

### _Commit changes of the files_ 
```git
git status
git add .
git commit -a -m "add new feature email-field to the employee (see #2)"
git push origin email-field
```

## Acceptance criteria 
- n/a
### - Generate all needed tests and verify that all tests passed
- Validate all attributes to pass null/empty values

### _Commit the tests of all employee attributes_ 
```git
git status
git add .
git commit -a -m "Tests created for the employee class (see #2)"
git push origin email-field
```


### _Merge email-field branch to the master_ 
```git
git checkout master
git merge email-field
git add .
git commit -a -m "email-field feature added to master (see #2)"
git push origin master
git tag -a v1.3.0 -m "email-field feature added v1.3.0 (see #2)"
git push origin v1.3.0
```

### _Create and change to the new branch fix-invalid-email_
```git
git checkout -b fix-invalid-email
```

### - Change the files to add the new field
- main/java/com.greglturnquist.payroll/
    - Employee 

### _Commit changes of the files_ 
```git
git status
git add .
git commit -a -m "add jobYears field (see #2)"
git push origin master
```

## Acceptance criteria 
- The email-field must only accept emails with @ sign
### - Generate all needed tests and verify that all tests passed
- Validate all attributes to pass null/empty values

### _Commit the tests of all employee attributes_ 
```git
git status
git add .
git commit -a -m "Tests created for the employee class (see #2)"
git push origin fix-invalid-email
```

<br>

## Build and RUN server on project folder
windows
```bash
mvnw spring-boot:run
```
linux
```bash
./mvnw spring-boot:run
```

<br>
Go to url

```url
https://localhost:8080
```

## Debug
- Verify if the application run without errors in your favourite browser
  - Inspect on browser console for errors

### _Commit changes and tag the new version_ 
```git
git status
git add .
git commit -a -m "fix email feature completed - tested and debugged (see #2)"
git push origin fix-invalid-email
git checkout master
git merge fix-invalid-email
git add .
git commit -a -m "fix email added to master - tested and debugged (see #2)"
git push origin master
git tag -a v1.3.1 -m "email feature fixed 1.3.1"
git push origin v1.3.1
git tag -a ca1-part2 -m "End of the assignement (close #2)"
git push origin ca1-part2
git ls-remote --tags origin
```

# <a id="part3"></a>

# Part 3 - Alternative Solution #
Alternative solution to the git (Version Control System)
I choose the Mercurial 

Motive:
- learning curve fast because is similar to the Git system
- is free too
- is compatible with windows

Server:
- Helix TeamHub [https://helixteamhub.cloud/](https://helixteamhub.cloud/)

Implementation last commits:

![alt helix_mercurial](helix_mercurial.png)


# <a id="part31"></a>

## Alternative Part 1 - Add new feature to the application with no branches
## Create employee new field to record years of the employee in the company
<br>


### _Go to folder_ 

_CA1 / tut-react-and-spring-data-rest / basic_

<br>

### _Mark application version v1.1.0_ (major.minor.revision)

```mercurial
hg tag v1.1.0
hg push
```

### - Change the files to add the new field
- main/java/com.greglturnquist.payroll/
    - DatabaseLoader
    - Employee 
- main/js/api/
    - app.js

### _Commit changes of the files_ 
```mercurial
hg status
hg add .
hg commit -m "fix-invalid-email (see #1)"
hg push
```

## Acceptance criteria 
- The field jobYears must only accept integer values
### - Generate all needed tests and verify that all tests passed
- Validate all attributes to pass null/empty values

### _Commit the tests of all employee attributes_ 
```mercurial
hg status
hg add .
hg commit -m "Tests created for the employee class (see #1)"
hg push
```

<br>

## Build and RUN server on project folder
windows
```bash
mvnw spring-boot:run
```
linux
```bash
./mvnw spring-boot:run
```

<br>
Go to url

```url
https://localhost:8080
```

## Debug
- Verify if the application run without errors in your favourite browser
  - Inspect on browser console for errors

### _Commit changes and tag the new version_ 
```mercurial
hg status
hg add .
hg commit -m "jobYears feature completed - tested and debugged (close #1)"
hg push
hg tag v1.2.0 
hg push
hg tag ca1-part1
hg push
```


<br><br>

# <a id="part32"></a>

# Part 2 - Add new feature to the application with branches
## Create employee new field email for the employee in the company

<br>

<br>

### _Go to folder_ 
_CA1 / tut-react-and-spring-data-rest / basic_

<br>

### _Create and change to the new branch email-field_
```mercurial
hg branch email-field
hg push --new-branch
hg up email-field
```

### _Verify actual working branch_
```mercurial
hg branch
```

### - Change the files to add the new field
- main/java/com.greglturnquist.payroll/
    - DatabaseLoader
    - Employee 
- main/js/api/
    - app.js

### _Commit changes of the files_ 
```mercurial
hg status
hg add .
hg commit -m "add new feature email-field to the employee (see #2)"
hg push 
```

## Acceptance criteria 
- n/a
### - Generate all needed tests and verify that all tests passed
- Validate all attributes to pass null/empty values

### _Commit the tests of all employee attributes_ 
```mercurial
hg status
hg add .
hg commit -m "Tests created for the employee class (see #2)"
hg push
```

### _Merge email-field branch to the master_ 
```mercurial
hg up default
hg merge email-field
hg add .
hg commit -m "email-field feature added to default branch (see #2)"
hg push
hg tag v1.3.0
hg push
```


### _Create and change to the new branch fix-invalid-email_
```mercurial
hg branch fix-invalid-email
hg push --new-branch
hg branch fix-invalid-email
```

### - Change the files to add the new field
- main/java/com.greglturnquist.payroll/
    - Employee 

### _Commit changes of the files_ 
```mercurial
hg status
hg add .
hg commit -m "fix-invalid-field (see #2)"
hg push
```

## Acceptance criteria 
- The email-field must only accept emails with @ sign
### - Generate all needed tests and verify that all tests passed
- Validate all attributes to pass null/empty values

### _Commit the tests of all employee attributes_ 
```mercurial
hg status
hg add .
hg commit -m "Tests created for the employee class (see #2)"
hg push
```

<br>

## Build and RUN server on project folder
windows
```bash
mvnw spring-boot:run
```
linux
```bash
./mvnw spring-boot:run
```

<br>
Go to url

```url
https://localhost:8080
```

## Debug
- Verify if the application run without errors in your favourite browser
  - Inspect on browser console for errors

### _Commit changes and tag the new version_ 
```mercurial
hg status
hg add .
hg commit -m "fix email feature completed - tested and debugged (see #2)"
hg push 
hg up default
hg merge fix-invalid-email
hg add .
hg commit -m "fix email added to master - tested and debugged (close #2)"
hg push
hg tag v1.3.1
hg push
hg tag ca1-part2
hg push
```