# DevOps 21/22 #
 
## Class Assignment
- [CA1 - Version Control with Git](CA1/readme.md)
- [CA2 - part 1 - Gradle](CA2/part1/readme.md)
- [CA2 - part 2 - Gradle](CA2/part2-gradle/readme.md)
- [CA2 - part 2 - Maven](CA2/part2-maven/readme.md)
- [CA3 - part 1 - Virtual Machine - Ubuntu](CA3/part1/readme.md)
- [CA3 - part 2 - Virtual Machines - Vagrant](CA3/part2/readme.md)
- [CA4 - part 1 - Container with Docker](CA4/part1/readme.md)
- [CA4 - part 2 - Container with Docker-composer](CA4/part2/readme.md)
- [CA4 - part 2 - Alternative - Kubernetes](CA4/part2-alternative-kubernetes/readme.md)
- [CA5 - part 1 - CI/CD Pipelines with Jenkins](CA5/part1/readme.md)
- [CA5 - part 2 - CI/CD Pipelines with Jenkins and docker](CA5/part2/readme.md)
- [CA5 - part 2 - Alternative - CI/CD Pipelines with Bitbucket](CA5/part2_alternative/readme.md)


---
Author
 
>@Joao Reis <1211775@isep.ipp.pt>
 
Teacher
 
>@Alexandre Bragança <atb@isep.ipp.pt>
 
---
## References
 
_Git_
 
>[Pro Git book](https://git-scm.com/book/en/v2)

>[Set up Git](https://docs.github.com/en/get-started/quickstart/set-up-git)

>[Git Feature Branch Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)

_Mercurial_

>[A tour of Mercurial: the basics](https://book.mercurial-scm.org/read/tour-basic.html)

_Markdown_

>[Markdown guide](https://www.markdownguide.org/basic-syntax/)

>[Markdown wikipedia](https://en.wikipedia.org/wiki/Markdown)

_Repository Hosting_

>[Bitbucket -  Git](https://bitbucket.org/)

>[Helix TeamHub - Git | Mercurial | Svn](https://www.perforce.com/products/helix-teamhub)

_Build tools_

>[Maven - Apache - plugins](https://maven.apache.org/plugins/index.html)

>[Gradle - plugins](https://docs.gradle.org/current/userguide/userguide.html)

_Virtual Machines_

_Vagrant_

>[Vagrant](https://www.vagrantup.com/)

>[Ruby - Vagrant language](http://www.ruby-lang.org/pt/documentation/)

_Vagrant Providers_

>[Virtualbox](https://www.virtualbox.org/)

>[Hyper-V](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)

>[VMware](https://www.vmware.com/)

_Kubernetes_

> [Kubernetes](https://kubernetes.io/)

> [Minikube](https://minikube.sigs.k8s.io/)

_CI/CD Pipelines_

> [Jenkins](https://www.jenkins.io/)

> [Bitbucket pipelines](https://bitbucket.org/)

> [Bamboo](https://www.atlassian.com/software/bamboo)

> [Gitlab CI](https://about.gitlab.com/)

> [CircleCI](https://circleci.com/)

> [Buddy](https://buddy.works/)
