# CA5 - CI/CD Pipelines with Jenkins and docker
in the Docker Hub. - part 2

![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange) ![alt badgedevops](https://img.shields.io/badge/devops--blue)


- [Part 2.1 - Install Jenkins](#part21)
- [Part 2.2 - Jenkins credentials ID](#part22)
- [Part 2.3 - Pipeline Script](#part23)
- [Part 2.4 - Pipeline Stage view](#part24)
- [Part 2.5 - Plugin styles issues](#part25)




![jenkins install](images/lojo_jenkins.jpg)



---

<br>

<a id="part21"></a> 

## Part 2.1

## Install Jenkins

Jenkins can be installed from the several ways, i choose via docker, after trying the others.
The instalation is almost the same as the other ways.

[Jenkins download page](https://jenkins.io/download)


<br>
### Via Docker
<em>Jenkins instalation an run it as a container </em>

```
docker run -u root --rm -d -p 8080:8080 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --name jenkins jenkinsci/blueocean
```

run on port 8090
```
docker run -d -p 8082:8080 --name testing  joaoreisweb/devops:28
```

---

<br>

<a id="part21"></a> 

## Part 2.1

## Install Jenkins plugins

- Jenkins Home page > pluginManager

> Javadoc Plugin - This plugin adds Javadoc support to Jenkins.

> HTML Publisher plugin - This plugin publishes HTML reports.

> JUnit - Allows JUnit-format test results to be published.


---

<br>

<a id="part23"></a> 

## Part 2.3

## Pipeline Script


```Jenkinsfile
pipeline {
    agent any
    environment { 
        registry = "joaoreisweb/jenkins_part2" 
        registryCredential = "hub-docker-id"
        dockerImage = ""
    }
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '8eb42d5b-2985-4180-8a76-02d0732baba5', 
                url: 'https://bitbucket.org/joaoreisweb/devops-21-22-atb-1211775'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir ("CA3/part2/basic") {
                    script {
                        if (isUnix())
                            sh '''
                            chmod +x gradlew
                            ./gradlew clean assemble
                            '''
                        else
                            bat './gradlew assemble'
                    }
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir ("CA3/part2/basic") {
                    script {
                        if (isUnix()){
                            sh './gradlew test'
                            junit 'build/test-results/test/*.xml'
                        }else{
                            bat './gradlew test'
                            junit 'build/test-results/test/*.xml'
                        }
                    }
                }
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Generating javadoc...'
                dir ("CA3/part2/basic") {
                    script {
                        javadoc javadocDir: 'build/reports/tests/test', keepAll: false
                    }
                }
            }
        }
        stage('HTML') {
            steps {
                echo 'documenting...'
                dir ("CA3/part2/basic"){
                    publishHTML([
                        allowMissing: false, 
                        alwaysLinkToLastBuild: false, 
                        keepAll: false, 
                        reportDir: 'build/reports/tests/test', 
                        reportFiles: 'index.html', 
                        reportName: 'HTML Report', 
                        reportTitles: 'Docs Loadtest Dashboard'
                        ])
                }
            }
        }
        stage('Archive') {
            steps {
                echo 'Archiving...'
                dir ("CA3/part2/basic"){
                    archiveArtifacts 'build/libs/*.war'
                }
            }
        }
        stage('Docker Image') {
            steps {
                echo 'Creating docker image...'
                script {
                    dir ("CA3/part2/basic"){
                        dockerImage = docker.build registry + ":${env.BUILD_ID}"
                    }
                }
            }
        }
        stage('Docker Deploy') { 
            steps { 
                echo 'Deploying docker image...'
                script { 
                    docker.withRegistry( '', registryCredential ) { 
                        dockerImage.push() 
                    }
                } 
            }
        }
    }
}
```


```Dockerfile
FROM tomcat:9.0.63

## build and deploy
COPY ./build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

## expose port to outside as public
EXPOSE 8080

```


---

<br>

<a id="part24"></a> 

## Part 2.4

## Pipeline Stage

![jenkins deploy](images/jenkins_build_with_deploy.jpg)




![jenkins deploy](images/jenkins_build_report.jpg)


---

<br>

<a id="part25"></a> 

## Part 2.5

## Plugin styles issues

On some plugins installed CSS is stripped out because of the Content Security Policy in Jenkins
You can fix this by using the groovy command

Manage Jenkins > Script console
```
System.setProperty("hudson.model.DirectoryBrowserSupport.CSP", "")
```