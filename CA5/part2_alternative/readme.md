# CA5 - CI/CD Pipelines with Bitbucket - part 2 - alternative

![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange) ![alt badgedevops](https://img.shields.io/badge/devops--blue)

- [Part 2.1 - Bitbucket pipelines](#part21)
- [Part 2.2 - Pipeline Script](#part22)
- [Part 2.3 - Bitbucket downloads](#part23)
- [Part 2.4 - Conclusion](#part24)



![bitbucket docker](images/logo_bitbucket_hub_docker.jpg)

---

<br>

<a id="part21"></a> 

## Part 2.1

## Bitbucket pipelines

Bitbucket Pipelines is an integrated CI/CD service built into Bitbucket. It allows you to automatically build, test, and even deploy your code based on a configuration file in your repository.

<em>Prerequisites:</em>
> - You need to have a Bitbucket Cloud account.
> - Your workspace must have at least one repository.

![bitbucket docker](images/bitbucket_pipeline.jpg)

Docker hub 
[hub.docker.com](hub.docker.com)
![bitbucket docker](images/bitbucket_hub_docker.jpg)


---

<br>

<a id="part22"></a> 

## Part 2.2

## Pipeline Script


A pipeline is defined using a YAML file called bitbucket-pipelines.yml, which is located at the root of your repository.

<em>bitbucket-pipelines.yml</em>

```yaml
image: gradle:7.4.1-jdk11

pipelines:
  default:
      - step:
          name: Checkout
          script:
            - git clone https://bitbucket.org/joaoreisweb/devops-21-22-atb-1211775
          artifacts:
            - devops-21-22-atb-1211775/CA3/part2/basic/**
      - step:
          name: Assemble
          caches:
            - gradle
          script:
            - cd devops-21-22-atb-1211775/CA3/part2/basic/
            - bash ./gradlew assemble
          artifacts:
            - devops-21-22-atb-1211775/CA3/part2/basic/**
      - step:
          name: Test
          script:
            - cd devops-21-22-atb-1211775/CA3/part2/basic/
            - bash ./gradlew test
          artifacts:
            - devops-21-22-atb-1211775/CA3/part2/basic/**
      - step:
          name: Javadoc
          script:
            - cd devops-21-22-atb-1211775/CA3/part2/basic/
            - bash ./gradlew javadoc
          artifacts:
            - devops-21-22-atb-1211775/CA3/part2/basic/**
      - step:
          name: Publish
          script:
            - cd devops-21-22-atb-1211775/CA3/part2/basic/
            - bash ./gradlew publish
          artifacts:
            - devops-21-22-atb-1211775/CA3/part2/basic/**
      - step:
          name: Archive
          script:
          - pipe: atlassian/bitbucket-upload-file:0.3.2
            variables:
              BITBUCKET_USERNAME: $BITBUCKET_USERNAME
              BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
              FILENAME: devops-21-22-atb-1211775/CA3/part2/basic/build/distributions/*
      - step:
          name: Docker Image Push
          script:
            - cd devops-21-22-atb-1211775/CA3/part2/basic/
            - docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
            - docker build -t $DOCKER_HUB_REPOSITORY:BB_$BITBUCKET_BUILD_NUMBER .
            - docker push $DOCKER_HUB_REPOSITORY:BB_$BITBUCKET_BUILD_NUMBER
          services:
            - docker
```

<em>Dockerfile</em>
```Dockerfile
FROM tomcat:9.0.63

## build and deploy
COPY ./build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

## expose port to outside as public
EXPOSE 8080
```



---

<br>

<a id="part23"></a> 

## Part 2.3

## Bitbucket downloads

Bitbucket downloads

Downloads lets you upload any file you want to your repository, be it public or private, after the upload is complete, your file will be accessible via the Cloudfront content delivery network (CDN). If a repository is private it will include an authentication token that is good for 24 hours, but that also means that your files are not accessible to anyone else. For public files, they will be served from:

cdn.bitbucket.org/joaoreisweb/ca5_part2_alternative/downloads/ 
(Data Center application UI is inaccessible)

[https://bitbucket.org/joaoreisweb/ca5_part2_alternative/downloads/](https://bitbucket.org/joaoreisweb/ca5_part2_alternative/downloads/)
(Public repository)

![bitbucket downloads](images/bitbucket_downloads.jpg)


---

<br>

<a id="part24"></a> 

## Part 2.4

## Conclusion

The bitbucket pipelines it is an Integrated continuous integration and continuous deployment for Bitbucket Cloud that's trivial to set up, automating your code from test to production.
It's not possible to configure multiple pipelines but there are a few workarounds that might work for your use case: Configure multiple parallel steps with each one running the build for a specific subdirectory.

**Alternatives:**
Jenkins, CircleCI, GitLab, GitLab CI, and Bamboo