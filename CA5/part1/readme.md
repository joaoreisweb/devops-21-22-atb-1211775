# CA5 - CI/CD Pipelines with Jenkins - part 1

![alt badgeisep](https://img.shields.io/badge/isep-2021/2022-orange) ![alt badgedevops](https://img.shields.io/badge/devops--blue)

- [Part 2.1 - Install Jenkins](#part21)
- [Part 2.2 - Jenkins credentials ID](#part22)
- [Part 2.3 - New pipeline and Checkout - Setup](#part23)
- [Part 2.4 - Pipeline dashboard - stage view](#part24)
- [Part 2.5 - Pipeline Script](#part25)



![jenkins install](images/lojo_jenkins.jpg)

---

<br>

<a id="part21"></a> 

## Part 2.1

## Install Jenkins

Jenkins can be installed from the several ways, i choose via docker, after trying the others.
The instalation is almost the same as the other ways.

[Jenkins download page](https://jenkins.io/download)

Working directory in different installations
![jenkins install](images/jenkins_workingdir.jpg)

<br>
### Via Docker
<em>Jenkins instalation an run it as a container </em>

Using a Docker Container
```
docker run -u root --rm -d -p 8080:8080 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --name jenkins jenkinsci/blueocean
```

Running directly the War file
```
java -jar jenkins.war
```

![jenkins install](images/install_jenkins.jpg)

After installation we need to create a admin user to access the Jenkins dashboard

![jenkins install](images/install_jenkinsv_auth.jpg)

<br>

---

<br>

<a id="part22"></a> 

## Part 2.2

## Jenkins credentials ID

Dashboard > User > Credentials

We have to create extra credentials, if we need to use external connections ( ex. git ).

![jenkins install](images/jenkins_credentials.jpg)


---

<br>

<a id="part23"></a> 

## Part 2.3

## New pipeline and Checkout - Setup

![jenkins install](images/jenkins_newjob.jpg)


Pipeline Job 
Configure > Pipeline

We have to add the credential id to the repository call

<em>Checkout - Via Jenkinsfile</em><br>

![jenkins install](images/jenkins_setup_git.jpg)

```Jenkinsfile
...
stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '8eb42d5b-2985-4180-8a76-02d0732baba5', 
                url: 'https://bitbucket.org/joaoreisweb/devops-21-22-atb-1211775'
            }
        }
        ...
```

<em>Checkout - Via Script</em><br>

```Jenkinsfile
...
stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/joaoreisweb/devops-21-22-atb-1211775'
            }
        }
        ...
```


---

<br>

<a id="part24"></a> 

## Part 2.4

## Pipeline dashboard - stage view

<br>

Via git or local there the same result.
Via remote we have commits information.

![jenkins install](images/jenkins_build_with_git.jpg)
![jenkins install](images/jenkins_build.jpg)

---

<br>

<a id="part25"></a> 

## Part 2.5

## Pipeline Script


```Jenkinsfile
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '8eb42d5b-2985-4180-8a76-02d0732baba5', url: 'https://bitbucket.org/joaoreisweb/devops-21-22-atb-1211775'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir ("CA2/part1/master") {
                    script {
                        if (isUnix())
                            sh '''
                            chmod +x gradlew
                            ./gradlew assemble
                            '''
                        else
                            bat './gradlew assemble'
                    }
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir ("CA2/part1/master"){
                    script {
                        if (isUnix())
                            sh './gradlew test'
                        else
                            bat './gradlew test'
                    }
                }
            }
        }
        stage('Archive') {
            steps {
                echo 'Archiving...'
                dir ("CA2/part1/master"){
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }
}
```