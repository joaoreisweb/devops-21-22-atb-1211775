# CA2 - Class Assignment 2 #
## Part 2

Build Tools with Gradle
===================

For the Part 2 of this assignment you should convert the basic version 
(i.e., "basic" folder) of the Tutorial application to Gradle (instead of Maven).


Create new gradle spring boot project

Go to [Spring initializr](https://start.spring.io)
Project Gradle Project
| Project Metadata | Group com.example |
| Artifact          | com.greglturnquist |

Name

Description

Package name


Dependencies
```gradle
dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-data-rest'
	implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
	runtimeOnly 'com.h2database:h2'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
}
```

<br>

## Task copy
build.grade
```gradle
task copy (type: Copy) {
    from fileTree(dir: 'build/libs', include: ['*.jar'])
    into 'dist'
}
```
Command line
```bash
./gradlew copy
```

## Task delete folder
build.grade
```gradle
task deleteBuiltFolder (type: Delete)  {
	delete 'src/main/resources/static/built'
}
```
Command line
```bash
./gradlew deleteBuiltFolder
```

## Task override clean
build.grade
```gradle
clean.configure  {
	dependsOn deleteBuiltFolder
}
```
Command line
```bash
./gradlew clean
```

## Task build
Command line
```bash
./gradlew build
```

## Run server
Command line
```bash
./gradlew bootRun
```
<br>
