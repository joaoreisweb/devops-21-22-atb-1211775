/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private String email;
	private int jobYears;

	protected Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, int jobYears, String email) {
		setFirstName(firstName);
		setLastName(lastName);
		setDescription(description);
		setJobTitle(jobTitle);
		setJobYears(jobYears);
		setEmail(email);
	}


	/*
	GET METHODS
	 */

	/**
	 * Get id of the employee
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Get first name of the employee
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Get last name of the employee
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Get the description of the employee
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Get job title of the employee
	 */
	public String getJobTitle() {
		return this.jobTitle;
	}

	/**
	 * Get job years of the employee
	 */
	public int getJobYears() {
		return this.jobYears;
	}

	/**
	 *
	 * Get email of the employee
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Set id for the employee
	 * @param id long
	 */
	private void setId(Long id) {

		this.id = id;
	}

	/*
	SET METHODS
	 */

	/**
	 * Set first name for the employee
	 * @param firstName String
	 */
	public void setFirstName(String firstName) {
		if(firstName.isEmpty() || firstName.isBlank()){
			throw new IllegalArgumentException("The field cannot be empty.");
		}
		this.firstName = firstName.trim();
	}

	/**
	 * Set last name for the employee
	 * @param lastName String
	 */
	public void setLastName(String lastName) {
		if(lastName.isEmpty() || lastName.isBlank()){
			throw new IllegalArgumentException("The field cannot be empty.");
		}
		this.lastName = lastName.trim();
	}

	/**
	 * Set description for the employee
	 * @param description String
	 */
	public void setDescription(String description) {
		if(description.isEmpty() || description.isBlank()){
			throw new IllegalArgumentException("The field cannot be empty.");
		}
		this.description = description.trim();
	}

	/**
	 * Set job title for the employee
	 * @param jobTitle String
	 */
	public void setJobTitle(String jobTitle) {
		if(jobTitle.isEmpty() || jobTitle.isBlank()){
			throw new IllegalArgumentException("The field cannot be empty.");
		}
		this.jobTitle = jobTitle.trim();
	}

	/**
	 * Set job years to the employee
	 * @param jobYears int job years
	 */
	public void setJobYears(int jobYears) {
		if(jobYears < 0){
			throw new IllegalArgumentException("Only integer values are allowed.");
		}
		this.jobYears = jobYears;
	}

	public void setEmail(String email) {

		if(email.isEmpty() || email.isBlank()){
			throw new IllegalArgumentException("The field cannot be empty.");
		}

		if(!checkEmail(email)){
			throw new IllegalArgumentException("The email is Invalid.");
		}

		this.email = email.trim();



	}

	protected boolean checkEmail(String email){
		// Java email validation permitted by RFC 5322
		String regex = "^(?=.{1,64}@)[A-Za-z0-9\\+_-]+(\\.[A-Za-z0-9\\+_-]+)*@"
				+ "[^-][A-Za-z0-9\\+-]+(\\.[A-Za-z0-9\\+-]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);

		return matcher.matches();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
				Objects.equals(firstName, employee.firstName) &&
				Objects.equals(lastName, employee.lastName) &&
				Objects.equals(description, employee.description) &&
				Objects.equals(jobTitle, employee.jobTitle) &&
				Objects.equals(email, employee.email) &&
				Objects.equals(jobYears, employee.jobYears);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, firstName, lastName, description, jobTitle, jobYears, email);
	}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
			", jobTitle='" + jobTitle + '\'' +
			", jobYears='" + jobYears + '\'' +
			", email='" + email + '\'' +
			'}';
	}
}
// end::code[]
