# CA2 - Class Assignment 2 #
## Part 2 - alternative

Build Tools with Maven
===================

For the Part 2 of this assignment with Maven.



pom.xml
```maven
<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-rest</artifactId>
		</dependency>
		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>
		...
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>
```

<br>


## Task copy directory
pom.xml
```maven
<plugin>
	<artifactId>maven-resources-plugin</artifactId>
	<version>3.0.2</version>
	<executions>
		<execution>
			<id>copy-resources</id>
			<phase>test</phase>
			<goals>
				<goal>copy-resources</goal>
			</goals>
			<configuration>
				<outputDirectory>${basedir}/backup/src</outputDirectory>
				<resources>
					<resource>
						<directory>src</directory>
						<filtering>true</filtering>
					</resource>
				</resources>
			</configuration>
		</execution>
	</executions>
</plugin>
```
Command line
```bash
./mvnw test
```

<br>

## Task copy directory filter files
pom.xml
```maven
<plugin>
	<artifactId>maven-resources-plugin</artifactId>
	<version>3.0.2</version>
	<executions>
		<execution>
			<id>copy-resources-jar</id>
			<phase>test</phase>
			<goals>
				<goal>copy-resources</goal>
			</goals>
			<configuration>
				<outputDirectory>${basedir}/backupJar</outputDirectory>
				<resources>
					<resource>
						<directory>target</directory>
						<filtering>true</filtering>
						<includes>
							<include>*.jar</include>
							<include>*.jar.original</include>
						</includes>
					</resource>
				</resources>
			</configuration>
		</execution>
	</executions>
</plugin>
```
Command line
```bash
./mvnw test
```

<br>

## Task delete directory
pom.xml
```maven
<plugin>
	<artifactId>maven-clean-plugin</artifactId>
	<version>3.1.0</version>
	<executions>
			<execution>
				<id>clean-built-folder</id>
				<phase>pre-clean</phase>
				<goals>
					<goal>clean</goal>
				</goals>
				<configuration>
					<filesets>
						<fileset>
							<directory>${basedir}/src/main/resources/static/built</directory>
							<followSymlinks>false</followSymlinks>
						</fileset>
					</filesets>
				</configuration>
		</execution>
	</executions>
</plugin>
```
Command line
```bash
./mvnw clean
```


## App install
Command line
```bash
./mvnw install
```

## Run server
Command line
```bash
./mvnw spring-boot:run
```
<br>
