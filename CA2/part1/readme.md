# CA2 - Class Assignment 2 #
## Part 1

Build Tools with Gradle
===================

For the Part 1 of this assignment you should download and commit to your
repository (in a folder for Part 1 of CA2) the example application available at
[link gradle_basic_demo](https://bitbucket.org/luisnogueira/gradle_basic_demo/)

<br>

---
## Add a new task to execute the server
build.grade
```gradle
task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a server on localhost:59001 "

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'
}
```
Command line
```bash
./gradlew runServer
```
<br>

---
## Add a new task to verify all tests
build.grade
```gradle
test {
    // Discover and execute JUnit4-based tests
    useJUnit()

    // show standard out and standard error of the test JVM(s) on the console
    testLogging.showStandardStreams = true

    // set heap size for the test JVM(s)
    minHeapSize = "128m"
    maxHeapSize = "512m"

    filter {
        //include all tests from package
        includeTestsMatching "basic_demo.*"
    }

    // listen to events in the test execution lifecycle
    beforeTest { descriptor ->
        logger.lifecycle("Running test: " + descriptor)
    }

    // show standard out and standard error of the test JVM(s) on the console
    testLogging.showStandardStreams = true
    testLogging {
        afterSuite { desc, result ->
            if (!desc.parent) { // will match the outermost suite
                println "Results: ${result.resultType} (${result.testCount} tests, ${result.successfulTestCount} successes, ${result.failedTestCount} failures, ${result.skippedTestCount} skipped)"
            }
        }
    }
}
```
Command line
```bash
./gradlew test
```
<br>

---
## Add a new task of type Copy
build.grade
```gradle
task copy (type: Copy) {
    from 'src'
    into 'backup'
}
```
Command line
```bash
./gradlew copy
```
<br>

---
## Add a new task of type Zip
build.grade
```gradle
task zip(type: Zip){

    archiveFileName = "backup.zip"

    // destination in build/backup folder
    // destinationDirectory = layout.buildDirectory.dir('backup')

    // destination in backupZip folder
    destinationDirectory = file("backupZip")

    from "src"
}
```
Command line
```bash
./gradlew zip
```

<br>


## Build ##
---
To build a .jar file with the application:

```bash
./gradlew build
```

## Run a server ##
---
Start the server:

```bash
./gradlew runServer
```

## Run a client ##
---
Open another terminal (one per user) and execute the following gradle task :
```bash
./gradlew runClient
```